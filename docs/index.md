Title: CSS pour Brief
Date:  2018-09-20
Updated: 2020-09-19
Cats:  web
Tags:  firefox, rss, css
Technos: css
State: stable
Intro: Petite feuille de style pour surcharger celle de [Brief](amo:brief), extension de Firefox qui permet de lire / s'abonner à des flux rss.

## Code

°°stx-css°°
    ..include:: /lab/brief-css/src/brief.css


## Historique

..include:: ./changelog.md

## Licence

..include:: ./licence.md


